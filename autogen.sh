# Calls gnome-autogen to build Makefiles and run configure

srcdir=`dirname $0`
test -z "$srcdir" && srcdir=.

REQUIRED_AUTOMAKE_VERSION=1.7.2
ACLOCAL_FLAGS="$ACLOCAL_FLAGS -I m4" USE_GNOME2_MACROS=1 USE_COMMON_DOC_BUILD=yes . gnome-autogen.sh
