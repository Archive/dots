#!/usr/bin/env python
# Dots - A braille translation program.
#
# Copyright (C) 2010 Consorcio Fernando de los Rios
#               Author: Fernando Herrera <fherrera@onirica.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# TODO:
#	* saving
#	* translate non UTF-8 entries
#	* check "=" in dots
#	* Hovers with intrustions (or label)
#	* i18n


import os
from sys import argv
from dots import host_settings
import pygtk
import gtk
import re
import tempfile
import shutil
import subprocess

import gettext
_ = gettext.gettext


opcodes = {
  "include": ["filename"],
  "locale": ["characters"],
  "capsign": ["dots"],
  "begcaps": ["dots"],
  "lenbegcaps": [],
  "endcaps": ["dots"],
  "firstwordcaps": [],
  "lastwordaftercaps": [],
  "lencapsphrase": [],
  "letsign": ["dots"],
  "noletsignbefore": ["characters"],
  "noletsign": ["letters"],
  "noletsignafter": ["characters"],
  "numsign": ["dots"],
  "firstwordital": ["dots"],
  "italsign": ["dots"],
  "lastworditalbefore": ["dots"],
  "lastworditalafter": ["dots"],
  "begital": ["dots"],
  "firstletterital": ["dots"],
  "endital": ["dots"],
  "lastletterital": ["dots"],
  "singleletterital": ["dots"],
  "italword": [],
  "lenitalphrase": ["number"],
  "firstwordbold": ["dots"],
  "boldsign": ["dots"],
  "lastwordboldbefore": ["dots"],
  "lastwordboldafter": ["dots"],
  "begbold": ["dots"],
  "firstletterbold": ["dots"],
  "endbold": ["dots"],
  "lastletterbold": ["dots"],
  "singleletterbold": ["dots"],
  "boldword": [],
  "lenboldphrase": ["number"],
  "firstwordunder": ["dots"],
  "undersign": ["dots"],
  "lastwordunderbefore": ["dots"],
  "lastwordunderafter": ["dots"],
  "begunder": ["dots"],
  "firstletterunder": ["dots"],
  "endunder": ["dots"],
  "lastletterunder": ["dots"],
  "singleletterunder": ["dots"],
  "underword": [],
  "lenunderphrase": ["number"],
  "begcomp": ["dots"],
  "compbegemph1": [],
  "compendemph1": [],
  "compbegemph2": [],
  "compendemph2": [],
  "compbegemph3": [],
  "compendemph3": [],
  "compcapsign": [],
  "compbegcaps": [],
  "compendcaps": [],
  "endcomp": ["dots"],
  "multind": ["dots" ,"opcode"],
  "compdots": [],
  "comp6": ["character", "dots"],
  "class": ["name", "character"],
#Special ons using opcode!
  "after": ["class", "opcode"],
  "before": ["class", "opcode"],
  "noback": ["opcode"],
  "nofor": ["opcode"],
  "swapcc": ["name", "characters", "dots..."],
  "swapcd": ["name", "dots...", "dotpattern..."],
  "swapdd": ["name", "characters", "characters"],
  "space": ["character", "dots"],
  "digit": ["character", "dots"],
  "punctuation": ["character", "dots"],
  "math": ["character", "dots"],
  "sign": ["character", "dots"],
  "letter": ["characters", "dots"],
  "uppercase": ["characters", "dots"],
  "lowercase": ["character", "dots"],
  "grouping": ["name", "characters", "dots,dots"],
  "uplow": ["characters", "dots[,dots]"],
  "litdigit": ["character", "dots"],
  "display": ["character", "dots"],
  "replace": ["characters", "characters"],
  "context": ["test", "action"],
  "correct": ["test", "action"],
  "pass2": ["test", "action"],
  "pass3": ["test", "action"],
  "pass4": ["test", "action"],
  "repeated": ["characters", "dots"],
  "repword": ["characters", "dots"],
  "capsnocont": [],
  "always": ["characters", "dots"],
  "exactdots": ["characters", "dots"],
  "nocross": ["characters", "dots"],
  "syllable": ["characters", "dots"],
  "nocont": ["characters"],
  "compbrl": ["characters"],
  "literal": ["characters"],
  "largesign": ["characters", "dots"],
  "word": ["characters", "dots"],
  "partword": ["characters", "dots"],
  "joinnum": ["characters", "dots"],
  "joinword": ["characters", "dots"],
  "lowword": ["characters", "dots"],
  "contraction": ["characters"],
  "sufword": ["characters", "dots"],
  "prfword": ["characters", "dots"],
  "begword": ["characters", "dots"],
  "begmidword": ["characters", "dots"],
  "midword": ["characters", "dots"],
  "midendword": ["characters", "dots"],
  "endword": ["characters", "dots"],
  "prepunc": ["characters", "dots"],
  "postpunc": ["characters", "dots"],
  "begnum": ["characters", "dots"],
  "midnum": ["characters", "dots"],
  "endnum": ["characters", "dots"],
  "decpoint": ["character", "dots"],
  "hyphen": ["character", "dots"],
  "nobreak": [] }


class Parser():
	def __init__ (self, filename):
		self.filename = filename

	def get_entries (self):
		lines = open(self.filename, 'r').readlines()

		entries = []
		for line in lines:
			line = line.lstrip(" ")
			line = line.replace("\t", " ")
			if line == '' or line[0] == '#' or line[0] == '<' or line[0] == '\n' or line[0] == '\r':
				continue
			sep = line.find(" ")
			entry = {}
			entry['opcode'] = line[:sep]
			entry['operands'] = line[sep+1:].rstrip(" \t\n\r")
			entries.append (entry)
		return entries




class EditDialog(gtk.Dialog):
	def __init__(self, opcode=None, operands=None):
		self.opcode_valid_regexs = {
			"character" : re.compile(r"^.$|(^\\(f?|n?|r?|s?|t?v?|e?)$)|^\\x(a|b|c|d|e|f|A|B|C|D|E|F|[0-9]){0,4}$|^\\y(a|b|c|d|e|f|A|B|C|D|E|F|[0-9]){0,5}$|^\\z(a|b|c|d|e|f|A|B|C|D|E|F|[0-9]){0,8}$"),
			"characters" : re.compile(r"(.|(\\(f?|n?|r?|s?|t?v?|e?))|\\x(a|b|c|d|e|f|A|B|C|D|E|F|[0-9]){0,4}|\\y(a|b|c|d|e|f|A|B|C|D|E|F|[0-9]){0,5}|\\z(a|b|c|d|e|f|A|B|C|D|E|F|[0-9]){0,8})+"),
			"filename" : re.compile(r".*"),
			"dots" : re.compile(r"(0|1|2|3|4|5|6|7|8|9|-|a|b|c|d|e|f)+"),
			"name" : re.compile(r"([a-z]*[A-Z]*)*"),
			"class" : re.compile(r"([a-z]*[A-Z]*)*"),
			"test" : re.compile(r".*"),
			"after" : re.compile(r"([a-z]*[A-Z]*)*"),
			"letters" : re.compile(r"([a-z]*[A-Z]*)*"),
			"number" : re.compile(r"[0-9]*"),
			"dots,dots" : re.compile(r"(0|1|2|3|4|5|6|7|8|9|-|a|b|c|d|e|f)+,(0|1|2|3|4|5|6|7|8|9|-|a|b|c|d|e|f)+"),
			"dots..." : re.compile(r"(0|1|2|3|4|5|6|7|8|9|-|a|b|c|d|e|f)+(,(0|1|2|3|4|5|6|7|8|9|-|a|b|c|d|e|f)+)*"),
			"dotpattern..." : re.compile(r"(0|1|2|3|4|5|6|7|8|9|-|a|b|c|d|e|f)+(,(0|1|2|3|4|5|6|7|8|9|-|a|b|c|d|e|f)+)*"),
			"dots[,dots]" : re.compile(r"(0|1|2|3|4|5|6|7|8|9|-|a|b|c|d|e|f)+(,(0|1|2|3|4|5|6|7|8|9|-|a|b|c|d|e|f)+)?"),
			#action
		}
		self.entries = []

		operands_list = []
		if operands is not None:
			operands_list = operands.split(" ")
			operands_list.reverse()
			if operands_list is None:
				operands_list = [ operands ]
		gtk.Dialog.__init__(self, "Edit op", None, gtk.DIALOG_MODAL | gtk.DIALOG_DESTROY_WITH_PARENT,
				    (gtk.STOCK_CANCEL, gtk.RESPONSE_REJECT, gtk.STOCK_OK, gtk.RESPONSE_ACCEPT))
		vbox = gtk.VBox (True, 6)

		self.add_opcode_combo(vbox, opcode)
		if opcode:
			comments = self.add_entries_for_opcode(vbox, opcode, operands_list)
			self.add_comments (vbox, comments)
		else:
			self.add_comments (vbox, [])
		self.get_content_area().add (vbox)
		self.show_all()

	def add_comments(self, vbox, comments):
		hbox = gtk.HBox (True, 6)
		label = gtk.Label ("Comments")
		entry = gtk.Entry()
		entry.set_data("opcode", "comments")
		self.entries.append(entry)
		hbox.pack_start (label)
		hbox.pack_start (entry)
		vbox.pack_start (hbox)
		if len(comments) > 0:
			comments.reverse()
			entry.set_text (" ".join (comments).lstrip(" "))


	def combo_changed(self, combo):
		remove = False
		for w in list(self.entries):
			vbox = w.get_parent().get_parent()
			hbox = w.get_parent()
			if remove:
				vbox.remove(hbox)
				self.entries.remove(w)
			if w == combo:
				remove = True
		opcode = combo.get_model().get_value(combo.get_active_iter(), 0)
		vbox = combo.get_parent().get_parent()
		self.add_entries_for_opcode (vbox, opcode, [])
		self.add_comments (vbox, [])
		vbox.show_all()

	def add_opcode_combo(self, vbox, opcode):
		label = gtk.Label ("opcode:")
		combo = gtk.ComboBox()
		model = gtk.ListStore (str)
		cell = gtk.CellRendererText()
		combo.pack_start(cell, True)
		combo.add_attribute(cell, 'text', 0)
		combo.set_model (model)
		for k in opcodes.keys():
			iter = model.append ([k])
			if k == opcode: combo.set_active_iter (iter)
		combo.connect("changed", self.combo_changed)
		hbox = gtk.HBox (True, 6)
		hbox.pack_start (label)
		hbox.pack_start (combo)
		vbox.add (hbox)
		self.entries.append(combo)


	def add_entries_for_opcode(self, vbox, opcode, operands_list):
		schema = opcodes[opcode]
		for s in schema:
			if s == "opcode":
				newop = operands_list.pop()
				self.add_opcode_combo(vbox, newop)
				self.add_entries_for_opcode(vbox, newop, operands_list)
			else:
				hbox = gtk.HBox (True, 6)
				label = gtk.Label (s)
				entry = gtk.Entry()
				entry.set_data("opcode", s)
				entry.connect("insert-text", self.validate_text)
				self.entries.append(entry)
				if len(operands_list) > 0:
					operand = ""
					while operand == "": operand = operands_list.pop()
					entry.set_text (operand)
				hbox.pack_start (label)
				hbox.pack_start (entry)
				vbox.pack_start (hbox)
				vbox.show_all()
		return operands_list




	def get_contents(self):
		operands = ""
		c = self.entries.pop(0)
		iter = c.get_active_iter()
		opcode = c.get_model().get_value(iter, 0)
		for e in self.entries:
			if isinstance(e, gtk.Entry):
				operands += e.get_text() + " "
			elif isinstance(e, gtk.ComboBox):
				iter = e.get_active_iter()
				operands += e.get_model().get_value(iter, 0) + " "
		return opcode, operands


	def validate_text(self, entry, string, len, position):
		opcode = entry.get_data("opcode")
		text = entry.get_text() + string
		if opcode == "opcode":
			for o in opcodes.keys():
				if o.find(text):
					return
		else:
			m = self.opcode_valid_regexs[opcode].match(text)
			if m:
				return

		entry.emit_stop_by_name("insert-text")



class TableEditor(gtk.Dialog):
	def __init__(self, parent=None):
		self.filename = None
		gtk.Dialog.__init__(self, "Edit op", None, gtk.DIALOG_MODAL | gtk.DIALOG_DESTROY_WITH_PARENT)
		self.set_title("Liblouis Table Editor")
		self.set_size_request(600, 600)
		self.liststore = gtk.ListStore(str, str)
		self.treeview = gtk.TreeView(self.liststore)
		tvcolumn = gtk.TreeViewColumn('opcode')
		self.treeview.append_column(tvcolumn)
		cell = gtk.CellRendererText()
		tvcolumn.pack_start(cell, True)
		tvcolumn.add_attribute(cell, 'text', 0)

		tvcolumn = gtk.TreeViewColumn('operands')
		self.treeview.append_column(tvcolumn)
		cell = gtk.CellRendererText()
		tvcolumn.pack_start(cell, True)
		tvcolumn.add_attribute(cell, 'text', 1)

		self.treeview.connect("row-activated", self.row_clicked)

		scrolledwindow = gtk.ScrolledWindow ()
		scrolledwindow.set_policy(gtk.POLICY_AUTOMATIC, gtk.POLICY_AUTOMATIC)
		scrolledwindow.add(self.treeview)

		vbox = self.get_content_area()
		buttonbox = gtk.HButtonBox()
		buttonbox.set_layout(gtk.BUTTONBOX_START)
		button = gtk.Button(_("_Add"))
		button.connect("clicked", self.add_clicked, self.treeview)
		buttonbox.pack_start(button, False, False, 6)
		button = gtk.Button(_("_Remove"))
		button.connect("clicked", self.remove_clicked, self.treeview)
		buttonbox.pack_start(button, False, False, 6)

		vbox.pack_start(scrolledwindow, True, True, 6)
		vbox.pack_start(buttonbox, False, False, 6)

		self.add_button(_("_Cancel editing"), gtk.RESPONSE_CANCEL)
		self.add_button(_("_Save table and use"), gtk.RESPONSE_OK)


	def _write_line(self, model, path, iter, f):
		line = model.get_value(iter, 0) + " " + model.get_value(iter, 1) + "\n"
		f.write(line)

	def save(self, file):
		f = open(file, 'w')
		self.liststore.foreach(self._write_line, f)
		f.close()

	def open(self, file):
		self.liststore.clear()
		parser = Parser (file)
		entries = parser.get_entries()
		for e in entries:
			iter = self.liststore.append ([e['opcode'], e['operands']])


	def save_as(self):
		chooser = gtk.FileChooserDialog(title="Save the table", action=gtk.FILE_CHOOSER_ACTION_SAVE,
				buttons=(gtk.STOCK_CANCEL,gtk.RESPONSE_CANCEL,gtk.STOCK_OPEN,gtk.RESPONSE_OK))
		response = chooser.run()
		if response == gtk.RESPONSE_OK:
			self.filename = chooser.get_filename()
			self.save(self.filename)
			path, file = os.path.split (self.filename)
			includes = self.get_includes()
			for i in includes:
				shutil.copy(host_settings.tablesdir + "/" + i, path)

		chooser.destroy()


		return response

	def add_clicked(self,button, view):
		dialog = EditDialog (None, None)
		dialog.set_transient_for(self)
		response = dialog.run()
		if response == gtk.RESPONSE_ACCEPT:
			model = view.get_model()
			opcode, operands = dialog.get_contents()
			iter = model.append((opcode, operands))
			selection = view.get_selection()
			selection.select_iter(iter)
			view.scroll_to_cell(model.get_path(iter))
		dialog.destroy()


	def remove_clicked(self,button, view):
		selection = view.get_selection();
		model, iter = selection.get_selected()
		model.remove(iter)

	def row_clicked(self, view, path, col):

		selection = view.get_selection();
		model, iter = selection.get_selected()
		opcode = model.get_value (iter, 0)
		operands = model.get_value (iter, 1)
		dialog = EditDialog (opcode, operands)
		dialog.set_transient_for(self)

		response = dialog.run()
		if response == gtk.RESPONSE_ACCEPT:
			opcode, operands = dialog.get_contents()
			model.set_value (iter, 0, opcode)
			model.set_value (iter, 1, operands)
		dialog.destroy()

	def get_filename(self):
		return self.filename

	def _get_include(self, model, path, iter, includes):
		if model.get_value(iter, 0) == "include":
			includes.append(model.get_value(iter, 1).partition(" ")[0])

	def get_includes(self):
		includes = []
		self.liststore.foreach(self._get_include, includes)
		return includes


	def test_table(self):
		tmpfile = tempfile.NamedTemporaryFile (delete=False)
		self.save(tmpfile.name)
		path, file = os.path.split (tmpfile.name)
		includes = self.get_includes()
		for i in includes:
			shutil.copy(host_settings.tablesdir + "/" + i, path)
		p = subprocess.Popen(args=["lou_checktable",tmpfile.name], stderr=subprocess.PIPE)
		p.wait()
		if p.returncode == 0:
			return "ok"
		else:
			return p.stderr.readline()

	def run(self):
		res = gtk.Dialog.run(self)
		if res != gtk.RESPONSE_OK:
			return res
		res = self.test_table()
		if res == "ok":
			res = self.save_as()
			if res == gtk.RESPONSE_OK:
				return gtk.RESPONSE_OK
			else:
				self.run()
		else:
			error = res.split(":", )
			dialog = gtk.MessageDialog(self, gtk.DIALOG_DESTROY_WITH_PARENT,
                                                   gtk.MESSAGE_ERROR, gtk.BUTTONS_CLOSE,
                                                   _("Error in table at line %(line)s: %(error)s") % {
                                                   'line': error[1], 'error': error[2]})
			dialog.run()
			dialog.destroy()
			path = str(int(error[1]) - 1)
			self.treeview.get_selection().select_path(path)
			self.treeview.scroll_to_cell(path)
			self.run()











if __name__ == "__main__":

	gettext.bindtextdomain(host_settings.DOMAIN,host_settings.LOCALE_DIR)
	gettext.textdomain(host_settings.DOMAIN)
	te = TableEditor()
	te.show_all()
	if len(argv) > 1:
		te.open(argv[1])

	gtk.main()


