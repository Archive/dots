# Dots - A braille translation program.
#
# Copyright (C) 2010 Consorcio Fernando de los Rios
#		Author: Fernando Herrera <fherrera@onirica.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

supported_mime_types = {}
supported_extensions_patterns = []

import mimetypes
from document import Document

from xmldocument import XmlDocument
supported_mime_types["application/xhtml+xml"] = XmlDocument
supported_mime_types["application/xml"] = XmlDocument
supported_mime_types["text/html"] = XmlDocument
supported_extensions_patterns.append("*.xhtml")
supported_extensions_patterns.append("*.xml")
supported_extensions_patterns.append("*.html")

from textdocument import TextDocument
supported_mime_types["text/plain"] = TextDocument
supported_extensions_patterns.append("*.text")
try:
	from odtdocument import OdtDocument
	supported_mime_types["application/vnd.oasis.opendocument.text"] = OdtDocument
	supported_extensions_patterns.append("*.odt")
except:
	pass

try:
	from pdfdocument_gi import PdfDocument
	supported_mime_types["application/pdf"] = PdfDocument
	supported_extensions_patterns.append("*.pdf")
except:
	try:
		from pdfdocument_pypoppler import PdfDocument
		supported_mime_types["application/pdf"] = PdfDocument
		supported_extensions_patterns.append("*.pdf")
	except:
		pass
	pass

try:
	from docdocument import DocDocument
	supported_mime_types["application/msword"] = DocDocument
	supported_extensions_patterns.append("*.doc")
	supported_extensions_patterns.append("*.dot")
except:
	pass



def document_new(filename):
	if filename is None:
		doc = TextDocument(None)
		return doc

	mime_type, encoding = mimetypes.guess_type (filename)
	if mime_type not in supported_mime_types:
		# Try text/* mimetypes
		if not mime_type or mime_type.startswith("text/"):
			doc = TextDocument(filename)
			return doc
		return None

	doc = supported_mime_types[mime_type] (filename)
	return doc

def get_supported_mime_types():
	return supported_mime_types.keys()

def get_supported_extensiones_patterns():
	return supported_extensions_patterns


if __name__ == "__main__":
	import sys
	from config_builder import ConfigBuilder

	if sys.argv[1] is None:
		print sys.argv[0] + " [file]"
	d = document_new (sys.argv[1])
	config_builder = ConfigBuilder()

	config_builder['xml']['semanticFiles'] = '*'
	config_builder['xml']['semanticFiles'] += ',nemeth.sem'
	config_builder['xml']['internetAccess'] = 'yes'
	config_builder['translation']['literaryTextTable'] = "Es-Es-g1.utb"
	config_builder['outputFormat']['cellsPerLine'] = 40
	config_builder['outputFormat']['braillePages'] = 'yes'
	config_builder['outputFormat']['formatFor'] = 'textDevice'
	config_builder['outputFormat']['LinesPerPage'] = 25
	config_builder['outputFormat']['braillePageNumberAt'] =  'bottom'

	d.translate (config_builder)
	print d.get_braille_text ()
