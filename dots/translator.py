# Dots - A braille translation program.
#
# Copyright (C) 2010 Consorcio Fernando de los Rios
#		Author: Fernando Herrera <fherrera@onirica.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import louisxml
import tempfile
import os


class Translator():
    def __init__(self, config):
        self.config = config

    def translate_file(self, input_file):
	f = open (input_file)
	res = self.translate_string (f.read())
	f.close()
	return res

    def translate_string(self, buffer):
	cfgfile = tempfile.NamedTemporaryFile (delete=False)
	cfgfile.write (str (self.config))
	cfgfile.close()
	res = louisxml.translateString (cfgfile.name, buffer, 0)
	os.unlink(cfgfile.name)
	return res

