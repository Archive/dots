# Dots - A braille translation program.
#
# Copyright (C) 2010 Consorcio Fernando de los Rios
#		Author: Fernando Herrera <fherrera@onirica.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from document import Document
from translator import Translator
import poppler

class PdfDocument(Document):
    def _get_text (self, file):
	uri = "file://" + file
	document = poppler.document_new_from_file (uri, None)
	npages = document.get_n_pages()
	text = ""
	for p in range(0,npages):
		page = document.get_page(p)
		w,h = page.get_size()
		r = poppler.Rectangle ()
		r.x1 = 0
		r.x2 = w
		r.y1 = 0
		r.y2 = h
		# Currently we are getting the layout from the pdf here
		# we should collapse it
		text += page.get_text(poppler.SELECTION_GLYPH,r)

	return text


    def translate(self, config):
	# FIXME: Check if poppler gives us always UTF-8 strings
	config['outputFormat']['inputTextEncoding'] = "UTF8"
	self.translator = Translator(config)
	text = self._get_text(self.input_file)
	self.braille_text = self.translator.translate_string (text)
	return



if __name__ == "__main__":
        import sys

        if len(sys.argv) > 1:
		document = PdfDocument(sys.argv[1])
		print document._get_text(sys.argv[1])


		PdfDocument._get_text


