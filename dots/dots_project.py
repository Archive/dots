# Dots - A braille translation program.
#
# Copyright (C) 2009 Eitan Isaacson
# Copyright (C) 2010 Consorcio Fernando de los Rios
#               Author: Fernando Herrera <fherrera@onirica.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
import os, tempfile, host_settings, sys, gtk
import ascii_braille
import gobject
import mimetypes

class DotsProject(gobject.GObject):
    __gsignals__ = { 'line-changed' : (gobject.SIGNAL_RUN_FIRST, gobject.TYPE_NONE, (gobject.TYPE_OBJECT, gobject.TYPE_STRING,)) }
    def __init__(self, document, name, view):
	gobject.GObject.__init__(self)
        self.view = view
	self.view.set_sensitive(True)
        self.buffer = gtk.TextBuffer()
        self.braille_buffer = gtk.TextBuffer()
	self.buffer.connect("notify", self._onBufferPropChanged)
	self.braille_buffer.connect("notify", self._onBufferPropChanged)
        self.document = document
        self.out_file = None
        self.config = None
	self.current_line = None

    def _onBufferPropChanged(self, buffer, paramspec):
	if paramspec.name != 'cursor-position':
		return
	mark = buffer.get_mark("insert")
	iter = buffer.get_iter_at_mark(mark)
	line = iter.get_line()
	start = self.buffer.get_iter_at_line(line)
	end = start.copy()
	end.forward_to_line_end()
	text = self.buffer.get_text (start, end)
	if text != self.current_line:
		self.current_line = text
		self.emit("line-changed", self, self.current_line)

    def transcribeBraille(self, config):
        self.config = config
        return self._transcribeBraille()

    def view_ascii(self):
        self.view.set_buffer(self.buffer)

    def view_braille(self):
        self.view.set_buffer(self.braille_buffer)

    def _transcribeBraille(self):
	self.document.translate (self.config)

        braille_text = self.document.get_braille_text()
	if braille_text == "":
		return False


	# Workaround for strange liblouis bug
	output = ""
	for c in braille_text:
		if (ord(c) != 0) and (ord(c) != 65535):
			output += c
	braille_text = output

        self.buffer.set_text(braille_text)

        self.braille_buffer.set_text(
            ''.join([ascii_braille.ascii_to_braille.get(
                        x, '') for x in braille_text]))

	begin = self.buffer.get_iter_at_offset(0)
	self.buffer.place_cursor(begin)
	begin = self.braille_buffer.get_iter_at_offset(0)
	self.braille_buffer.place_cursor(begin)
	return True

gobject.type_register(DotsProject) 
