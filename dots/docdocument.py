# Dots - A braille translation program.
#
# Copyright (C) 2010 Consorcio Fernando de los Rios
#		Author: Fernando Herrera <fherrera@onirica.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import os
import subprocess
from document import Document
from translator import Translator

def get_antiword():
	for path in os.environ["PATH"].split(os.pathsep):
            f = os.path.join(path, "antiword")
            if os.path.exists(f) and os.access(f, os.X_OK):
                return f
	return None

antiword = get_antiword()
if antiword is None:
	raise NameError('Antiword not found')

class DocDocument(Document):

    def _get_text(seff, file):
	text = subprocess.check_output([antiword, "-x", "db", file])
	return text

    def translate(self, config):
	config['outputFormat']['inputTextEncoding'] = "UTF8"
	self.translator = Translator(config)
	result = self._get_text (self.input_file)
	self.braille_text = self.translator.translate_string (result)

if __name__ == "__main__":
        import sys

        if len(sys.argv) > 1:
                document = OdtDocument(sys.argv[1])
                print document._get_text(sys.argv[1])


