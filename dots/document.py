# Dots - A braille translation program.
#
# Copyright (C) 2010 Consorcio Fernando de los Rios
#		Author: Fernando Herrera <fherrera@onirica.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from translator import Translator

class Document():
    def __init__(self, input_file):
        self.input_file = input_file
	self.braille_text = None
	self.output_file = None

    def set_output_file (self, output_file):
	self.output_file = output_file

    def get_braille_text(self):
	return self.braille_text
