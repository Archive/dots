# Dots - A braille translation program.
#
# Copyright (C) 2009 Eitan Isaacson
# Copyright (C) 2010 Fernando Herrera
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

DEFAULT_SEMANTICS = "*,nemeth.sem"
DEFAULT_TABLE = "Es-Es-g1.utb"
DEFAULT_CELLS = 40
DEFAULT_LINES = 25
DEFAULT_FORMAT = "textDevice"
DEFAULT_PAGEPOS = "bottom"

class ConfigBuilder(dict):
    def __init__(self):
        self['xml'] = _ConfigSection()
        self['translation'] = _ConfigSection()
        self['outputFormat'] = _ConfigSection()

    def __str__(self):
        s = ''
        for key in self.keys():
            s += '%s\n' % key
            for k,v in self[key].items():
                s += '\t%s %s\n' % (k ,v)
        return s

    def load_from_gconf(self, client):
	value = client.get_string ("/apps/dots/semanticFiles")
	if value is not None:
        	self['xml']['semanticFiles'] = value
	else:
		self['xml']['semanticFiles'] = DEFAULT_SEMANTICS

        if client.get_bool ("/apps/dots/internetAccess"):
                self['xml']['internetAccess'] = 'yes'
        else:
                self['xml']['internetAccess'] = 'no'

	value = client.get_string ("/apps/dots/literaryTextTable")
	if value is not None:
        	self['translation']['literaryTextTable'] = value
	else:
		self['translation']['literaryTextTable'] = DEFAULT_TABLE

	value = client.get_int ("/apps/dots/cellsPerLine")
	if value > 0:
        	self['outputFormat']['cellsPerLine'] = value
	else:
        	self['outputFormat']['cellsPerLine'] = DEFAULT_CELLS

        if client.get_bool ("/apps/dots/braillePages"):
                self['outputFormat']['braillePages'] = 'yes'
        else:
                self['outputFormat']['braillePages'] = 'no'

	value = client.get_string ("/apps/dots/formatFor")
	if value is not None:
        	self['outputFormat']['formatFor'] = value
	else:
        	self['outputFormat']['formatFor'] = DEFAULT_FORMAT

	value = client.get_int ("/apps/dots/LinesPerPage")
	if value > 0:
        	self['outputFormat']['LinesPerPage'] = value
	else:
        	self['outputFormat']['LinesPerPage'] = DEFAULT_LINES

	value = client.get_string ("/apps/dots/braillePageNumberAt")
	if value is not None:
        	self['outputFormat']['braillePageNumberAt'] = value
	else:
        	self['outputFormat']['braillePageNumberAt'] = DEFAULT_PAGEPOS


    def save_to_gconf(self, client):
        client.set_string ("/apps/dots/semanticFiles", self['xml']['semanticFiles'])
        if self['xml']['internetAccess'] == 'yes':
                client.set_bool ("/apps/dots/internetAccess", True)
        else:
                client.set_bool ("/apps/dots/internetAccess", False)
        client.set_string ("/apps/dots/literaryTextTable", self['translation']['literaryTextTable'])
        client.set_int ("/apps/dots/cellsPerLine", self['outputFormat']['cellsPerLine'])
        if self['outputFormat']['braillePages'] == 'yes':
                client.set_bool ("/apps/dots/braillePages", True)
        else:
                client.set_bool ("/apps/dots/braillePages", False)
        client.set_string ("/apps/dots/formatFor", self['outputFormat']['formatFor'])
        client.set_int ("/apps/dots/LinesPerPage", self['outputFormat']['LinesPerPage'])
        client.set_string ("/apps/dots/braillePageNumberAt", self['outputFormat']['braillePageNumberAt'])

class _ConfigSection(dict):
    pass

if __name__ == "__main__":
    cb = ConfigBuilder()
    cb['xml']['semanticFiles'] = '*,nemeth.sem'
    cb['xml']['internetAccess'] = 'yes'
    cb['translation']['literaryTextTable'] = 'en-us-g2.ctb'
    cb['outputFormat']['cellsPerLine'] = 40
    cb['outputFormat']['linesPerPage'] = 25

    print str(cb)
