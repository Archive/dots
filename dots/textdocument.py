# Dots - A braille translation program.
#
# Copyright (C) 2010 Consorcio Fernando de los Rios
#		Author: Fernando Herrera <fherrera@onirica.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from document import Document
from translator import Translator

class TextDocument(Document):

    def set_text(self, text):
	self.text = text;

    def translate(self, config):
	self.translator = Translator(config)
	if self.input_file is not None:
		self.braille_text = self.translator.translate_file (self.input_file)
	else:
		self.braille_text = self.translator.translate_string (self.text)


