# Dots - A braille translation program.
#
# Copyright (C) 2009 Eitan Isaacson
# Copyright (C) 2010 Consorcio Fernando de los Rios
#               Author: Fernando Herrera <fherrera@onirica.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import pygtk
import gtk, glib
import os, tempfile
import subprocess
import host_settings
import gettext
import gconf
import gtkunixprint
import pango
from dots_project import DotsProject
from document_builder import document_new
from document_builder import get_supported_mime_types
from document_builder import get_supported_extensiones_patterns
from config_builder import ConfigBuilder
from table_editor import TableEditor
from translator import Translator

import gettext
gettext.bindtextdomain(host_settings.DOMAIN,host_settings.LOCALE_DIR)
gettext.textdomain(host_settings.DOMAIN)
_ = gettext.gettext

class AppWindow(object):
    def __init__(self):
	self.project = None
	self.document = None
	self.table_group = None
        self.main_xml = gtk.Builder()
	self.main_xml.set_translation_domain(host_settings.DOMAIN)
        self.main_xml.add_from_file (
            os.path.join(host_settings.gtkbuilder_dir, 'app_window.xml'))
        self.window = self.main_xml.get_object('window1')
	self.window.set_icon_name('dots')
        self.main_xml.connect_signals(self)
        self.config_builder = ConfigBuilder()
	self.client = gconf.client_get_default ()
	self.client.add_dir ("/apps/dots", gconf.CLIENT_PRELOAD_NONE)
        self.conf_xml = gtk.Builder()
	self.conf_xml.set_translation_domain(host_settings.DOMAIN)
        self.conf_xml.add_from_file (
            os.path.join(host_settings.gtkbuilder_dir, 'config.xml'))

        view = self.main_xml.get_object('textview1')
	view.modify_font(pango.FontDescription('Mono'))
	self.loadConfig()

    def loadConfig(self):
	self.config_builder.load_from_gconf(self.client)
	self._populateTablesMenu()

    def saveConfig(self):
	self.config_builder.save_to_gconf(self.client)

    def _OnTranslationFormatActivate(self, item):
	dialog = self.conf_xml.get_object('format_dialog')
	dialog.set_transient_for(self.window)
	radiotop = self.conf_xml.get_object('page_top')
	radiobottom = self.conf_xml.get_object('page_bottom')
	radionone = self.conf_xml.get_object('page_none')

	if self.config_builder['outputFormat']['braillePages'] == "yes":
		if self.config_builder['outputFormat']['braillePageNumberAt'] == "top":
			radiotop.set_active(True)
		else:
			radiobottom.set_active(True)
	else:
		radionone.set_active(True)
	lines = self.conf_xml.get_object('lines_entry')
	lines.set_value (self.config_builder['outputFormat']['LinesPerPage'])
	cells = self.conf_xml.get_object('cells_entry')
	cells.set_value (self.config_builder['outputFormat']['cellsPerLine'])

	response = dialog.run()
	if response == 0:
		self.config_builder['outputFormat']['cellsPerLine'] = cells.get_value_as_int()
		self.config_builder['outputFormat']['LinesPerPage'] = lines.get_value_as_int()
		if radiotop.get_active():
			self.config_builder['outputFormat']['braillePages'] = "yes"
			self.config_builder['outputFormat']['braillePageNumberAt'] = "top"
		elif radiobottom.get_active():
			self.config_builder['outputFormat']['braillePages'] = "yes"
			self.config_builder['outputFormat']['braillePageNumberAt'] = "bottom"
		else:
			self.config_builder['outputFormat']['braillePages'] = "no"

		if self.document is not None:
			self.translate (self.document, self.config_builder)

	dialog.hide()

    def _OnTranslationEditTableActivate(self, item):
	te = TableEditor (self.window)
	table = self.config_builder['translation']['literaryTextTable']
	if table.startswith('/'):
		te.open(table)
	else:
		te.open(os.path.join(host_settings.tablesdir, table))
	te.set_transient_for(self.window)
	te.show_all()
	while True:
		res = te.run()
		if res == gtk.RESPONSE_OK:
			new_table = te.get_filename()
			path, file = os.path.split (new_table)
			os.chdir(path)
			eitem = self._addTable (self.submenu, file, True)
			eitem.activate()
			eitem.show()
			break
		else:
			break
	te.destroy()




    def _addTable(self, submenu, table, prepend=False):
	tableitem = gtk.RadioMenuItem(self.table_group, table)
	tableitem.connect("activate", self._onTableActivate, table)
	if prepend:
		submenu.prepend (tableitem)
	else:
		submenu.append (tableitem)
	self.table_group = tableitem
	return tableitem

    def _onTableOtherActivate(self, item):
	chooser = gtk.FileChooserDialog(title=None,action=gtk.FILE_CHOOSER_ACTION_OPEN,
					buttons=(gtk.STOCK_CANCEL,gtk.RESPONSE_CANCEL,gtk.STOCK_OPEN,gtk.RESPONSE_OK))
 	response = chooser.run()
	filename = chooser.get_filename()
	chooser.destroy()
	if response == gtk.RESPONSE_OK:
		path, file = os.path.split (filename)
		os.chdir (path)
		#table_ok = self._testTable(filename)
		#if not table_ok:
		#	dialog = self.main_xml.get_object("table_error_dialog")
		#	dialog.set_transient_for(self.window)
		#	dialog.run()
		#	dialog.hide()
		#	return

		eitem = self._addTable (self.submenu, file, True)
		eitem.activate()
		eitem.show()

    def _onTableActivate(self, item, table):
	old_table = self.config_builder['translation']['literaryTextTable']
	self.config_builder['translation']['literaryTextTable'] = table
	p = subprocess.Popen(args=["lou_checktable",table], stderr=subprocess.PIPE)
        p.wait()
        if p.returncode != 0:
		dialog = self.main_xml.get_object("table_error_dialog")
		dialog.set_transient_for(self.window)
		dialog.run()
		dialog.hide()
		self.config_builder['translation']['literaryTextTable'] = old_table
		return


	if self.document is not None:
		self.translate (self.document, self.config_builder)



    def _populateTablesMenu(self):
        def _sepatatorFunc(model, itr):
            return model[itr][0] == None
        menuitem = self.main_xml.get_object('select_table_menuitem')
	#Fixme, dont make this global, use get_menu at _onTableOtherActivate
	self.submenu = gtk.Menu()
	menuitem.set_submenu(self.submenu)
        table_list = filter(lambda x: x.endswith('ctb') or x.endswith('utb'),
                            os.listdir(host_settings.tablesdir))
        table_list.sort()
	group = None
	found = False
        for table in table_list:
		item = self._addTable (self.submenu, table)
		if table == self.config_builder['translation']['literaryTextTable']:
			item.activate()
			found = True

	othermenu = gtk.MenuItem("Other...")
	othermenu.connect_after("activate", self._onTableOtherActivate)
	self.submenu.append(othermenu)

	if not found:
		if os.access (self.config_builder['translation']['literaryTextTable'], os.R_OK):
			eitem = self._addTable (self.submenu, self.config_builder['translation']['literaryTextTable'], True)
			eitem.activate()
		else:
			item = self.submenu.get_active()
			item.activate()



    def _onOpen(self, action):
	chooser = gtk.FileChooserDialog(title=None,action=gtk.FILE_CHOOSER_ACTION_OPEN,
					buttons=(gtk.STOCK_CANCEL,gtk.RESPONSE_CANCEL,gtk.STOCK_OPEN,gtk.RESPONSE_OK))
	filter = gtk.FileFilter()
	filter.set_name(_("All files"))
	filter.add_pattern("*")
	chooser.add_filter(filter)

	filter = gtk.FileFilter()
	filter.set_name(_("Documents"))
	for m in get_supported_mime_types():
		filter.add_mime_type(m)
	for p in get_supported_extensiones_patterns():
		filter.add_pattern(p)
	chooser.add_filter(filter)
	chooser.set_filter(filter)
 	response = chooser.run()
	filename = chooser.get_filename()
	chooser.destroy()
	if response == gtk.RESPONSE_OK:
		self.document = document_new (filename)
		# TODO: Check for None here, although we should not get
		#       non-supported mime-types from the filechooser
		self.addDocument (self.document)
		self.translate (self.document, self.config_builder)

    def _onNew(self, action):
	view = self.main_xml.get_object('textview1')
	view.set_sensitive(True)
	view.set_editable(True)
	self.document = document_new (None)
	self.addDocument (self.document)
	action = self.main_xml.get_object('action_translate')
	action.set_sensitive(True)
	self.statusbar = self.main_xml.get_object('statusbar1')
	context_id = self.statusbar.get_context_id("text file")
	self.statusbar.push(context_id, _("Text editable"))
	view.grab_focus()

    def _onTranslate(self, action):
	# We are translating the text buffer, so set it to the document
	action = self.main_xml.get_object('action_translate')
	action.set_sensitive(False)
	context_id = self.statusbar.get_context_id("text file")
	self.statusbar.pop(context_id)
	view = self.main_xml.get_object('textview1')
	view.set_editable(False)
	buffer = view.get_buffer()
	self.document.set_text (buffer.get_text(
                                self.project.buffer.get_start_iter(),
                                self.project.buffer.get_end_iter()))
	self.translate(self.document, self.config_builder)
	
	
    def _OnRevisionLine(self, item):
	reviewentry = self.main_xml.get_object('reviewentry')
	if item.get_active():
        	reviewentry.show()
	else:
        	reviewentry.hide()


    def _OnBrailleViewToggle(self, ascii_item):
        if ascii_item.get_active():
            self.project.view_ascii()
        else:
            self.project.view_braille()

    def _onLineChanged(self, view, project, line):
	self.main_xml.get_object('reviewentry').set_text(line)


    def _onSave(self, action):
        if self.project.out_file is None:
            self._onSaveAs(action)
        else:
            fsave = open(self.project.out_file, 'w')
            fsave.write(self.project.buffer.get_text(
                    self.project.buffer.get_start_iter(),
                    self.project.buffer.get_end_iter()))
            fsave.close()
            self.project.buffer.set_modified(False)

    def _onSaveAs(self, action):
        dialog = gtk.FileChooserDialog(parent=self.window,
            action=gtk.FILE_CHOOSER_ACTION_SAVE,
            buttons=(gtk.STOCK_CANCEL, gtk.RESPONSE_CANCEL,
                     gtk.STOCK_SAVE, gtk.RESPONSE_OK))

        if self.project.out_file:
            dialog.set_filename(self.project.out_file)

        if (dialog.run() == gtk.RESPONSE_OK):
            fsave = open(dialog.get_filename(), 'w')
            fsave.write(self.project.buffer.get_text(
                    self.project.buffer.get_start_iter(),
                    self.project.buffer.get_end_iter()))
            fsave.close()
            self.project.out_file = dialog.get_filename()
            self.project.buffer.set_modified(False)

        dialog.destroy()

    def _printJobSent(self, job, data, errormsg):
	if errormsg is not None:
		print "Error printing: " + errormsg

	os.unlink(data)


    def _onPrint(self, action):
	dialog = gtkunixprint.PrintUnixDialog()
        dialog.set_transient_for(self.window)
	response = dialog.run()
	if response == gtk.RESPONSE_OK:
		printer = dialog.get_selected_printer()
		settings = dialog.get_settings()
		setup = dialog.get_page_setup()
		job = gtkunixprint.PrintJob("dots", printer, settings, setup)
		tmpfile = tempfile.NamedTemporaryFile (delete=False)
		tmpfile.write (self.project.buffer.get_text(
				self.project.buffer.get_start_iter(),
				self.project.buffer.get_end_iter()))
		tmpfile.close()
		job.set_source_file(tmpfile.name)
		job.send(self._printJobSent, tmpfile.name)
	dialog.destroy()

    def _onAbout(self, action):
	about = gtk.AboutDialog()
	about.set_name ("dots")
	about.set_comments(_("A Braille translator for GNOME"))
	about.set_license("GPLv3")
	about.set_authors(["Eitan Isaacson", "Fernando Herrera  <fherrera@onirica.com>"])
	about.set_copyright("(C) 2009 Eitan Isaacson\n"
                            "(C) 2010 Consorcio Fernando de los Rios\n"
                            "(C) 2010 Guadalinfo.es\n")
	about.set_website("http://live.gnome.org/Dots")
	about.set_website_label("Dots website")
	about.set_documenters(["Fernando Herrera"])
	about.set_translator_credits(_("translator-credits"))
        link = gtk.LinkButton("http://guadalinfo.es", "(C) 2010 Guadalinfo.es")
        about.get_content_area().pack_start(link, False, False, 0)
	link.show()
	about.run()
	about.destroy()


    def addDocument(self, document):
	if self.project is not None:
		del self.project
        self.project = DotsProject(document, _("Unsaved Document"), self.main_xml.get_object("textview1"))
	ascii_radio = self.main_xml.get_object("ascii_radio")
	if ascii_radio.get_active():
            self.project.view_ascii()
        else:
            self.project.view_braille()
	if document.input_file is not None:
		self.window.set_title("Dots - " + os.path.basename(document.input_file))
	else:
		self.window.set_title("Dots")
	self.project.connect("line-changed", self._onLineChanged)

    def translate(self, document, config):
        res = self.project.transcribeBraille(config)
	if not res:
		dialog = self.main_xml.get_object('notext-dialog')
		dialog.run()
		dialog.hide()

	action = self.main_xml.get_object('action_save_as')
	action.set_sensitive(res)
	action = self.main_xml.get_object('action_save')
	action.set_sensitive(res)
	action = self.main_xml.get_object('action_print')
	action.set_sensitive(res)

	return res


    # FIXME: this should be doable in glade
    def _add_control_key(self, widget, accel_group, key):
	widget.add_accelerator("activate", accel_group, ord(key),
                               gtk.gdk.CONTROL_MASK, gtk.ACCEL_VISIBLE)

    def _add_shift_control_key(self, widget, accel_group, key):
	widget.add_accelerator("activate", accel_group, ord(key),
                               gtk.gdk.SHIFT_MASK | gtk.gdk.CONTROL_MASK, gtk.ACCEL_VISIBLE)



    def _menu_add_accels(self):
	accel_group = gtk.AccelGroup()
	self.window.add_accel_group(accel_group)
	self._add_control_key(self.main_xml.get_object('menu_new'), accel_group, 'n')
	self._add_control_key(self.main_xml.get_object('menu_open'), accel_group, 'o')
	self._add_control_key(self.main_xml.get_object('menu_save'), accel_group, 's')
	self._add_shift_control_key(self.main_xml.get_object('menu_save_as'), accel_group, 's')
	self._add_control_key(self.main_xml.get_object('menu_print'), accel_group, 'p')
	self._add_control_key(self.main_xml.get_object('menu_quit'), accel_group, 'q')
	self._add_control_key(self.main_xml.get_object('menu_translate'), accel_group, 't')
	self._add_control_key(self.main_xml.get_object('ascii_radio'), accel_group, 'a')
	self._add_control_key(self.main_xml.get_object('braille_radio'), accel_group, 'b')
	self._add_control_key(self.main_xml.get_object('line_check'), accel_group, 'l')
	self._add_control_key(self.main_xml.get_object('menu_format'), accel_group, 'f')
	self.main_xml.get_object('menu_help').add_accelerator("activate", accel_group, gtk.keysyms.F1, 0, gtk.ACCEL_VISIBLE)

    def run(self):
        self.window.show_all()
        self.main_xml.get_object('reviewentry').hide()
	self._menu_add_accels()
        gtk.main()

    def _onHelp(self, action):
	gtk.show_uri(None, "ghelp:dots", gtk.gdk.CURRENT_TIME)

    def _onQuit(self, window, event=None):
	self.saveConfig()
        gtk.main_quit()

if __name__ == "__main__":
    window = AppWindow()
    window.show_all()
    window.main_xml.get_object('reviewentry').hide()
    gtk.main()
